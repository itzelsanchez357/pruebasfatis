package com.example.nuestracalcu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Double r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSuma = (Button)findViewById(R.id.btnSuma);
        Button btnResta = (Button)findViewById(R.id.btnResta);
        Button btnMult = (Button)findViewById(R.id.btnMult);
        Button btnDivir = (Button)findViewById(R.id.btnDividir);

        final TextView textMostarR = (TextView)findViewById(R.id.textMostarR);
        final EditText editUno = (EditText) findViewById(R.id.editUno);
        final EditText editDos = (EditText) findViewById(R.id.editDos);

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMostarR.setText(Double.parseDouble(editUno.getText().toString()) +
                        Double.parseDouble(editDos.getText().toString())+"");
            }
        });
        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMostarR.setText(Double.parseDouble(editUno.getText().toString()) -
                        Double.parseDouble(editDos.getText().toString())+"");
            }
        });
        btnDivir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMostarR.setText(Double.parseDouble(editUno.getText().toString()) /
                        Double.parseDouble(editDos.getText().toString())+"");
            }
        });
        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMostarR.setText(Double.parseDouble(editUno.getText().toString()) *
                        Double.parseDouble(editDos.getText().toString())+"");
            }
        });

    }

}